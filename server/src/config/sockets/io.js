var io = null;

function socketIOConfig(server) {
    if(io && !server) return io;

    io = require('socket.io')(server);

    require.main.require('./src/api/sockets/connection')(io);

    return io;
}

module.exports = socketIOConfig;