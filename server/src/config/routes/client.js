var router = require('express').Router();
var User = require.main.require('./src/api/models/User');
var Appointment = require.main.require('./src/api/models/Appointment');
var noCache = require.main.require('./src/utils/nocache');
var path = require('path');

var _ = require('lodash');

router.get('/user/:user_id', noCache, function(req, res) {
    var userId = req.params.user_id;

    if(!userId) {
        res.status(400).json({
            message: 'Tried to search for nothing.'
        });

        return;
    }

    User.findOne({_id: userId}, function(err, user) {
        if(err) {
            res.status(404).json({
                message: err
            });

            return;
        }

        res.status(200).json({
            user: user.toJson()
        });
    });
});

router.post('/user-setup', function(req, res) {
    var requiredFields = ['firstname', 'lastname', 'address', 'city', 'country', 'county', 'zipcode'];
    var hasRequiredFields = _.every(requiredFields, _.partial(_.has, req.body));

    if(!hasRequiredFields) res.status(400).json({
        message: 'Unable to complete profile setup. Are you missing a field?'
    });

    User.findById(req.body._id, function(err, user) {
        if(!user) {
            res.status(404).json({
                message: err
            });

            return;
        }

        user = _.extend(user, req.body, {setup: true, fullname: req.body.firstname + ' ' + req.body.lastname});
        user.save(function(saveErr) {
            if(saveErr) {
                res.status(500).json({
                    message: "There's a problem on the server. User could not be saved."
                });

                return;
            }

            res.status(200).json({
                user: user.toJson()
            });
        });
    });
});

router.get('/doctors', function(req, res) {
     User.find({isDoctor: true, googleServices: { $exists: true }, _id: {$ne: req.session.user._id} }, function(err, doctors) {
        if(err) {
            res.status(500).json({
                message: err
            });

            return;
        }

        res.status(200).json({
            doctors: _.map(doctors, function(doctor) {
                return doctor.toJson();
            })
        });
     });
});

router.get('/upcoming-appointments', function(req, res) {
    var user = req.session.user;

    Appointment.find({userId: user._id, start: { $gte: new Date() }})
        .populate('doctor').exec().then(function(appointments) {
        if(!appointments.length) {
            console.log(path.basename(__filename) + ": No appointments found.");
            res.status(200).json({
                success: true,
                message: "No upcoming appointments."
            });
            return;
        }

        res.status(200).json({
            success: true,
            message: "You have " + appointments.length + " upcoming " + (appointments.length === 1 ? "appointment." : "appointments."),
            appointments: appointments
        });
    });
});

module.exports = router;