var router = require('express').Router();
var createQRCode = require.main.require('./src/api/services/qrcode/qrCodeGenerator');
var crypto = require('crypto');
var User = require.main.require('./src/api/models/User');
var Appointment = require.main.require('./src/api/models/Appointment');
var Promise = require('bluebird');
var path = require('path');
var _ = require('lodash');
var moment = require('moment');

var verificationFilesRoot = path.resolve(__dirname, '../../../../client/app/build');

router.get('/generate/:userId/:doctorId/:start', function(req, res, next) {
    var userId = req.params.userId;
    var doctorId = req.params.doctorId;
    var start = req.params.start;

    var userPromise = new Promise(function(resolve, reject) {
        User.findOne({_id: userId}).exec().then(function(user) {
            if(!user) {
                console.error(__filename + ": User with ID " + userId + " not found");
                next("Could not retrieve user.");
                return;
            }

            resolve(user);
        });
    });

    var doctorPromise = new Promise(function(resolve, reject) {
        User.findOne({_id: doctorId}).exec().then(function(doctor) {
            if(!doctor) {
                console.error(__filename + ": Doctor with ID " + doctorId + " not found");
                next("Could not retrieve doctor.");
                return;
            }

            resolve(doctor);
        });
    });

    Promise.all([userPromise, doctorPromise]).then(function(data) {
        var en0Network = require('os').networkInterfaces()['en0'];
        var ip = _.find(en0Network, {family: 'IPv4', internal: false}).address;
        console.log(ip);

        var user = data[0];
        var doctor = data[1];
        var baseURI = req.protocol + '://' + ip + ':' + req.socket.localPort + '/qrcode/check/';
        var toHash = [user._id, doctor._id, doctor.secret, start].join(':');
        var hash = crypto.createHash('md5').update(toHash).digest('hex');
        var codeStream = createQRCode(baseURI + hash, 'png');

        console.log('toHash: ' + toHash);
        console.log('hash: ' + hash);

        console.log(baseURI + toHash);
        codeStream.pipe(res);
    });
});

router.get('/check/:identifier', function(req, res) {
    Appointment.findOne({identifier: req.params.identifier}).exec().then(function(appointment) {
        if(!appointment) {
            console.log('Appointment not found. Identifier: ' + req.params.identifier);

            res.sendFile("invalid.html", {
                root: verificationFilesRoot
            });

            return;
        }

        var end = moment(appointment.end);
        if(end.isBefore(new Date())) {
            console.log('Appointment expired.');
            res.sendFile("expired.html", {
                root: verificationFilesRoot
            });

            return;
        }


        res.sendFile("valid.html", {
            root: verificationFilesRoot
        });
    });
});

exports = module.exports = router;