var router = require('express').Router(),
    User = require.main.require('./src/api/models/User'),
    jwt = require('jwt-simple'),
    moment = require('moment'),
    passport = require('passport');

router.post('/signup', function(req, res, next) {
    passport.authenticate('local-register', function(err, user, info) {
        if(err) { return next(err); }
        if(!user) {
            res.status(401).json({
                success: false,
                message: info.message
            });

            return;
        }

        req.session.user = user;
        successResponse(user, res, createToken(user, req.hostname), 'Successfully registered.');
    })(req, res, next);
});

router.post('/login', function(req, res, next) {
    passport.authenticate('local-login', function(err, user, info) {
        if(err) {
            console.log(err.message);
            failResponse(res, 500, err.message);
            return;
        }

        if(!user) {
            console.log(info.message);
            failResponse(res, 401, info.message);
            return;
        }

        req.login(user, function(loginError) {
            if(loginError) {
                console.log(loginError.message);
                failResponse(res, 401, loginError.message);
                return;
            }

            req.session.user = user;
            successResponse(user, res, createToken(user, req.hostname), 'Welcome, ' + req.user.firstname);
        });
    })(req, res, next);
});

router.get('/logout', function(req, res) {
     req.session.destroy();
});

function createToken(user, hostname) {
    var payload = {
        iss: hostname,
        sub: user._id,
        exp: moment().add(10, 'days').unix()
    };

    return jwt.encode(payload, 'temp_secret_key');
}

function successResponse(user, res, token, message) {
    res.status(200).json({
        user: user.toJson(),
        token: token,
        success: true,
        message: message
    });
}

function failResponse(res, code, message) {
    res.status(code).json({
        success: false,
        message: message
    });
}

module.exports = router;