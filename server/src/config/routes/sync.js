var router = require('express').Router();
var SyncCalendar = require.main.require('./src/api/models/SyncCalendar');
var handleAPIError = require.main.require('./src/utils/handleAPIError');
var _ = require('lodash');

router.get('/synchronized-events', function(req, res) {
    var userId = req.query.userId;
    var doctorId = req.query.doctorId;
    
    var syncCal = { doctorId: doctorId, userId: userId };
    
    SyncCalendar.findOne(syncCal).exec().then(function(syncCalendar) {
        if(!syncCalendar) {
            res.status(200).json({
                success: false,
                message: "Could not retrieve synchronized events.",
                events: []
            });

            return;
        }

            var events = _.filter(syncCalendar.events, function(event) {
            var privateExtendedProps = _.get(event, 'extendedProperties.private', {});
            return privateExtendedProps.isDoctorEvent &&
                (privateExtendedProps.bookedBy !== userId);
        });

        res.status(200).json({
            success: true,
            events: events
        });
    });
});

module.exports = router;