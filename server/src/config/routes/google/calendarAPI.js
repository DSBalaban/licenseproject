var router = require('express').Router();
var path = require('path');
var _ = require('lodash');
var Promise = require('bluebird');

var calendarService = require.main.require('./src/api/services/google/calendar');
var Calendar = require.main.require('./src/api/models/Calendar');
var SyncCalendar = require.main.require('./src/api/models/SyncCalendar');
var calendarAPI = calendarService.calendarAPI;
var getOAuth2 = require.main.require('./src/utils/getOAuth2');
var handleAPIError = require.main.require('./src/utils/handleAPIError');

router.post('/medtime-calendar/init', function(req, res) {
    var oauth2Client = calendarService.getAuthClient();
    var user = req.body.user;
    var tokens = user.googleServices;

    Calendar.findOne({userId: user._id}, function(err, calendar) {
        if(calendar) {
            res.status(200).json({
                calendar: calendar
            });

            return;
        }

        oauth2Client.setCredentials({
            refresh_token: tokens.refreshToken
        });

        var resource = { summary: 'MedTime' };
        calendarAPI.calendars.insert({resource: resource, auth: oauth2Client}, function(err, resource) {
            if(err) {
                res.status(400).json({
                    message: err
                });

                return;
            }

            var calendar = new Calendar(_.merge(resource, {userId: user._id}));

            calendar.save(function(saveErr) {
                if (saveErr) {
                    res.status(500).json({
                        message: "There's a problem on the server. Calendar not saved."
                    });

                    return;
                }

                res.status(200).json({
                    calendar:  calendar
                });
            });
        });
    });
});

router.post('/medtime-calendar/sync', function(req, res) {
    var googleAPIError = require.main.require('./src/utils/gAPIError');

    var doctor = req.body.doctor;
    var doctorOAuth2 = getOAuth2(doctor.googleServices);

    var user = req.body.user;

    var doctorEventsPromise = new Promise(function(resolve, reject) {
        Calendar.findOne({userId: doctor._id}).exec().then(function(doctorCal) {
            if(!doctorCal) {
                calendarNotFound(res, doctor._id, reject);
                return;
            }

            console.log('Retrieving calendar ' + doctorCal.id + ' for userId ' + doctor._id);
            calendarAPI.events.list({calendarId: doctorCal.id, auth: doctorOAuth2}, function(err, doctorEvents) {
                if(err) {
                    googleAPIError(res, err, reject);
                    return;
                }

                resolve(doctorEvents);
            });
        });
    });

    doctorEventsPromise.then(function(events) {
        var syncCal = { doctorId: doctor._id, userId: user._id };
        SyncCalendar.findOne(syncCal).exec().then(function(syncedCalendar) {
            if(!syncedCalendar) {
                syncedCalendar = new SyncCalendar(syncCal);
            }

            syncedCalendar.events = _.filter(events.items, function(event) {
                var privateExtendedProps = _.get(event, 'extendedProperties.private', {});
                return privateExtendedProps.isDoctorEvent &&
                    (privateExtendedProps.bookedBy !== user._id);
            });
            
            syncedCalendar.save(function(saveErr) {
                if(saveErr) {
                    handleAPIError(saveErr, res);
                    return;
                }

                res.status(200).json({
                    success: true,
                    message: 'Synchronization successful.'
                });
            });
        });
    });
});

function calendarNotFound(res, id, reject) {
    var error = 'Calendar with owner ID ' + id + ' not found.';
    console.log(error);
    res.status(404).json({
        success: false,
        message: error
    });

    if(reject && _.isFunction(reject)) reject(error);
}

module.exports = router;