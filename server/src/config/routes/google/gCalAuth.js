var router = require('express').Router();
var _ = require('lodash');
var socketIO = require.main.require('./src/config/sockets/io')();
var calendarService = require.main.require('./src/api/services/google/calendar');

var goToConsentPageClient = calendarService.clientConsentPage;
var goToConsentPageServer = calendarService.serverConsentPage;
var oauth2Client = calendarService.oauth2Client;

var User = require.main.require('./src/api/models/User');

router.get('/calendar-permissions', function (req, res) {
    goToConsentPageServer(req, res);
});

router.get('/calendar-allowed', function (req, res) {
    var userId = _.get(req, ['session', 'user', '_id']);

    if (!userId) console.error('Session ID not found.');
    User.findById(userId, function (err, user) {
        if (!user) {
            res.status(404).json({
                message: err
            });

            return;
        }

        user.googleServices.calendarAuthCode = req.query.code;
        oauth2Client.getToken(req.query.code, function(err, tokens) {
            if(!err) {
                user.googleServices.accessToken = tokens.access_token;
                user.googleServices.refreshToken = tokens.refresh_token;
            }

            user.save(function (saveErr) {
                if (saveErr || err) {
                    res.status(500).json({
                        message: "There's a problem on the server. User could not be updated."
                    });

                    socketIO.to(user._id).emit('calendar-auth-error', {error: saveErr || err});
                }

                // res.redirect('/#/calendar');
                console.log('Emitting to ' + user._id);
                socketIO.to(user._id).emit('calendar-auth-success', {user: user});
            });
        });
    });
});

module.exports = router;