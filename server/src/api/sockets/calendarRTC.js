var _ = require('lodash');

// User
var User = require.main.require('./src/api/models/User');

// Appointment
var Appointment = require.main.require('./src/api/models/Appointment');

// Calendar
var calendarService = require.main.require('./src/api/services/google/calendar');
var Calendar = require.main.require('./src/api/models/Calendar');
var SyncCalendar = require.main.require('./src/api/models/SyncCalendar');
var calendarAPI = calendarService.calendarAPI;
var getOAuth2 = require.main.require('./src/utils/getOAuth2');
var sendConfirmationEmail = require.main.require('./src/utils/emailer/confirmation/sendConfirmationEmail');

var Promise = require('bluebird');

function calendarRTC(socket, io) {
    socket.on('calendar-connecting', function (data) {
        var user   = data.user;
        var tokens = user.googleServices;

        if (!_.hasIn(user, 'googleServices.accessToken')) {
            io.to(user._id).emit('calendar-unauthorized', {
                success: false,
                message: 'Could not validate Google credentials.'
            });
            return;
        }

        var oauth2Client = getOAuth2(tokens);

        console.log('Retrieving calendar for userId: ' + user._id);
        Calendar.findOne({userId: user._id}).exec().then(function (calendar) {
            if (!calendar) {
                var error = "Couldn't find calendar for " + user._id;
                console.log(error);
                io.to(user._id).emit('calendar-events-retrieval-error', error);
                return;
            }

            calendarAPI.events.list({calendarId: calendar.id, auth: oauth2Client}, function (gErr, resource) {
                if (gErr) {
                    io.to(user._id).emit({
                        success: false,
                        message: "There was a problem connecting to Google Calendar."
                    });

                    console.log('error retrieving calendar ' + calendar.id + ' in calendarRTC.js');
                    return;
                }

                socket.emit('calendar-connected', resource);
            });
        }).catch(function (err) {
            console.log(err);
        });
    });

    socket.on('event-insert', function (data) {
        var user   = data.user;
        var tokens = user.googleServices;
        var events = data.data;

        var oauth2Client   = getOAuth2(tokens);
        var eventsInserted = [];

        Calendar.findOne({userId: user._id}).exec().then(function (calendar) {
            var syncEvents = [];
            _.each(events, function (event) {
                var promise = new Promise(function (resolve, reject) {
                    _.delay(function () {
                        var resource = createResourceFromEvent(event, [user.email], user._id);

                        calendarAPI.events.insert({
                            calendarId: calendar.id,
                            resource: resource,
                            auth: oauth2Client
                        }, function (err, gEvent) {
                            if (err) {
                                console.error(err);
                                io.to(user._id).emit('event-insert-error', err);
                            } else {
                                if (event.isDoctorEvent) syncEvents.push(gEvent);
                                io.to(user._id).emit('event-insert-success', gEvent);
                            }

                            resolve();
                        });
                    }, 200);
                });

                eventsInserted.push(promise);
            });

            if (!events[0].isDoctorEvent) return;

            Promise.all(eventsInserted).then(function () {
                SyncCalendar.find({doctorId: user._id}).exec().then(function (syncCalendars) {
                    _.each(syncCalendars, function (syncCal) {
                        syncCal.events = syncCal.events.concat(syncEvents);
                        syncCal.save(function (saveErr) {
                            if (!saveErr) io.to(syncCal.userId).emit('events-receive', {
                                data: syncEvents,
                                notify: true
                            });
                        });
                    });
                });
            });
        });
    });

    socket.on('events-remove', function (data) {
        var user = data.user;
        var tokens = user.googleServices;
        var oauth2Client = getOAuth2(tokens);
        var event = data.data;

        Calendar.findOne({userId: user._id}).exec().then(function (calendar) {
            if (!calendar) {
                console.error('No calendar with userId ' + user._id + ' found');
                return;
            }

            SyncCalendar.find({doctorId: user._id}).exec().then(function (syncCalendars) {
                var payload = {
                    calendarId: calendar.id,
                    eventId: event.googleId,
                    auth: oauth2Client,
                    sendNotifications: !!syncCalendars.length && (event.isDoctorEvent.toString() === "true")
                };

                calendarAPI.events.delete(payload, function (err) {
                    if (err) {
                        console.error(err.message);
                        return;
                    }

                    io.to(user._id).emit('events-remove', {
                        data: event,
                        notify: false
                    });

                    _.each(syncCalendars, function (syncCal) {
                        syncCal.events = _.filter(syncCal.events, function (syncEvent) {
                            return syncEvent.id !== event.googleId;
                        });
                        syncCal.save(function (saveErr) {
                            if (!saveErr) io.to(syncCal.userId).emit('events-remove', {
                                data: event,
                                notify: true
                            });
                        });
                    });
                });
            });
        });
    });

    socket.on('events-edit', function (data) {
        var user         = data.user;
        var tokens       = user.googleServices;
        var oauth2Client = getOAuth2(tokens);
        var event        = data.data;

        Calendar.findOne({userId: user._id}).exec().then(function (calendar) {
            if (!calendar) {
                console.error('Calendar for user ' + user._id + ' not found');
                return;
            }

            var resource = {
                summary: event.title,
                start: {dateTime: event.start},
                end: {dateTime: event.end},
                isDoctorEvent: event.isDoctorEvent
            };

            resource = createResourceFromEvent(resource, [user.email], user._id);

            SyncCalendar.find({doctorId: user._id}).exec().then(function (syncCalendars) {
                if (syncCalendars.length === 0) {
                    console.log('No sync calendars with doctorId ' + user._id + ' found');
                }

                var payload = {
                    calendarId: calendar.id,
                    eventId: event.googleId,
                    auth: oauth2Client,
                    resource: resource
                };

                calendarAPI.events.update(payload, function (err) {
                    if (err) {
                        console.error(err.message);
                        return;
                    }

                    io.to(user._id).emit('events-edit', {
                        data: event,
                        notify: false
                    });

                    _.each(syncCalendars, function (syncCal) {
                        var syncEventIndex = _.findIndex(syncCal.events, function (syncEvent) {
                            return syncEvent.id === event.googleId;
                        });

                        syncCal.events[syncEventIndex] = event;
                        syncCal.save(function (saveErr) {
                            if (!saveErr) io.to(syncCal.userId).emit('events-edit', {
                                data: event,
                                notify: true
                            });
                        });
                    });
                });
            });
        });
    });

    socket.on('events-schedule', function(data) {
        var user = data.user;
        var event = data.data;
        var doctorId = event.owner;

        User.findOne({_id: doctorId}).exec().then(function(doctor) {
            if(!doctor) {
                console.error('Doctor ' + doctorId + ' not found');
                io.to(user._id).emit('There is currently a problem synchronizing with this doctor.');
                return;
            }

            Calendar.findOne({userId: doctorId}).exec().then(function(calendar) {
                if(!calendar) {
                    console.error('Calendar for doctor ' + doctorId + ' not found CRTC:L214');
                    io.to(user._id).emit('There is currently a problem scheduling events for this doctor.');
                    return;
                }

                event = _.assign(event, {
                    isDoctorEvent: true,
                    booked: true,
                    bookedBy: user._id
                });

                var resource = {
                    summary: event.title,
                    start: {dateTime: event.start},
                    end: {dateTime: event.end},
                    isDoctorEvent: true,
                    booked: true,
                    bookedBy: user._id
                };


                resource = createResourceFromEvent(resource, [user.email, doctor.email], doctorId);

                SyncCalendar.find({doctorId: doctorId}).exec().then(function(syncCalendars) {
                    if (syncCalendars.length === 0) {
                        console.log('No sync calendars with doctorId ' + user._id + ' found');
                    }

                    var payload = {
                        calendarId: calendar.id,
                        eventId: event.googleId,
                        auth: getOAuth2(doctor.googleServices),
                        resource: resource
                    };

                    var promise = new Promise(function(resolve, reject) {
                        calendarAPI.events.update(payload, function (err, gEvent) {
                            if(err) {
                                console.error(err.message);
                                io.to(user._id).emit('Could not update the event. Please try again or contact support.');
                                reject();
                                return;
                            }

                            resolve(gEvent);

                            io.to(doctorId).emit('events-schedule', event);

                            _.each(syncCalendars, function(syncCal) {
                                var syncEventIndex = _.findIndex(syncCal.events, function (syncEvent) {
                                    return syncEvent.id === event.googleId;
                                });

                                var syncCalEvents = [].concat(syncCal.events);
                                syncCalEvents[syncEventIndex] = gEvent;
                                syncCal.events = syncCalEvents;
                                syncCal.save(function (saveErr) {
                                    if (!saveErr) io.to(syncCal.userId).emit('events-schedule', event);
                                });
                            });
                        });
                    });

                    promise.then(function(gEvent) {
                        Calendar.findOne({userId: user._id}).exec().then(function(userCalendar) {
                            if(!userCalendar) {
                                console.error("Calendar for " + user._id + " not found CRTC:L281");
                                return;
                            }

                            calendarAPI.events.insert({
                                calendarId: userCalendar.id,
                                resource: gEvent,
                                auth: getOAuth2(user.googleServices)
                            }, function(err) {
                                if(err) console.error(err.message);
                                
                                var appointment = new Appointment({
                                    name: event.title,
                                    userId: user._id,
                                    doctor: doctor._id,
                                    location: doctor.location,
                                    start: event.start,
                                    end: event.end
                                });

                                appointment.save(doctor.secret, function(appointmentSaveErr, savedAppointment) {
                                    if(appointmentSaveErr) console.err(appointmentSaveErr.message);
                                    sendConfirmationEmail(user.email, savedAppointment, socket.handshake.headers.referer);
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

function createResourceFromEvent(event, attendees, owner) {
    var resource = _.assign(event, {
        attendees: attendees,
        reminders: {
            useDefault: false,
            overrides: [
                {method: 'email', 'minutes': 24 * 60}
            ]
        },
        extendedProperties: {
            'private': {
                owner: owner
            }
        }
    });

    if (event.isDoctorEvent) resource.extendedProperties.private.isDoctorEvent = true;
    if (event.booked) resource.extendedProperties.private.booked = true;
    if (event.bookedBy) resource.extendedProperties.private.bookedBy = event.bookedBy;

    return resource;
}

module.exports = calendarRTC;