function connection(io) {
    io.on('connection', function(socket) {
        handleConnection(socket, io);
    });
}

function handleConnection(socket, io) {
    socket.on('join', function(user) {
        socket.join(user._id);
        console.log(user._id + ' joined!');
        io.to(user._id).emit('connected', {message: 'Welcome'});
    });

    require.main.require('./src/api/sockets/calendarRTC')(socket, io);
}

module.exports = connection;