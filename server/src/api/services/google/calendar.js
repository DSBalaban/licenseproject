var google = require('googleapis');
var OAuth2 = google.auth.OAuth2;

var client= require.main.require('./client_secret.json').installed;
var oauth2Client = new OAuth2(client.client_id, client.client_secret, client.redirect_uris[1]);
var scopes = [
    'https://www.googleapis.com/auth/calendar'
];

var calendar = google.calendar({ version: 'v3' });

var url = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: scopes
});

function goToConsentPageClient(req, res) {
    res.status(200).json({
        consentURL: url
    });
}

function goToConsentPageServer(req, res) {
    res.redirect(url);
}

function getAuthClient() {
    return new OAuth2(client.client_id, client.client_secret, client.redirect_uris[1]);
}

exports.clientConsentPage = goToConsentPageClient;
exports.serverConsentPage = goToConsentPageServer;
exports.calendarAPI = calendar;
exports.oauth2Client = oauth2Client;
exports.getAuthClient = getAuthClient;
