var qr = require('qr-image');

// returns stream
function generate(source, type) {
    return qr.image(source, { type: type });
}

exports = module.exports = generate;