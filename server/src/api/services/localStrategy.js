var User = require.main.require('./src/api/models/User'),
    LocalStrategy = require('passport-local').Strategy;

var strategyOptions = {
    usernameField: 'email'
};

exports.login = new LocalStrategy(strategyOptions, function(email, pwd, done) {
    var userToSearch = { email: email };
    var credentialsError = { message: 'Invalid credentials.'};

    User.findOne(userToSearch, function(err, user) {
        if(err) return done(err);
        if(!user) return done(null, false, credentialsError);

        user.comparePasswords(pwd, function(err, isMatch) {
            if(err) return done(err);
            if(!isMatch) return done(null, false, credentialsError);

            return done(null, user);
        });
    });
});

exports.register = new LocalStrategy(strategyOptions, function(email, password, done) {
    var userToSearch = { email: email };
    var alreadyExistsError = { message: 'Email already exists.' };

    User.findOne(userToSearch, function(err, user) {
        if(err) return done(err);
        if(user) return done(null, false, alreadyExistsError);

        var newUser = new User({
            email: email,
            password: password
        });

        newUser.save(function(err, savedUser) {
            if(err) return done(err);

            console.log(savedUser._id);
            done(null, savedUser);
        });
    });
});