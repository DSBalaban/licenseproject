var mongoose = require('mongoose');

var calendarSchema = new mongoose.Schema({
    id: String,
    kind: String,
    etag: String,
    summary: String,
    description: String,
    location: String,
    timezone: String,
    userId: String
});

module.exports = mongoose.model('Calendar', calendarSchema);