var mongoose = require('mongoose'),
    bcrypt = require('bcrypt');

var userSchema = new mongoose.Schema({
    email: String,
    password: String,
    firstname: String,
    lastname: String,
    fullname: String,
    address: String,
    city: String,
    county: String,
    zipcode: String,
    country: String,
    setup: Boolean,
    isDoctor: Boolean,
    field: String,
    location: String,
    secret: String,
    googleServices: {
        calendarAuthCode: String,
        accessToken: String,
        refreshToken: String
    }
});

userSchema.pre('save', function(next) {
    var user = this;

    if(!user.isModified('password')) return next();

    bcrypt.genSalt(10, function(err, salt) {
        if(err) return next(err);

        bcrypt.hash(user.password, salt, function(err, hash) {
            if(err) return next(err);

            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePasswords = function(password, callback) {
    bcrypt.compare(password, this.password, callback);
};

userSchema.methods.toJson = function() {
    var user = this.toObject();
    delete user.password;

    return user;
};

module.exports = mongoose.model('User', userSchema);