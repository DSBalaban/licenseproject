var mongoose = require('mongoose');
var crypto = require('crypto');
var moment = require('moment');

var appointmentSchema = new mongoose.Schema({
    name: String,
    userId: String,
    doctor: { type: mongoose.Schema.ObjectId, ref: 'User' },
    location: String,
    identifier: String,
    start: Date,
    end: Date
});

appointmentSchema.pre('save', function(next, secret, callback) {
    var toHash = [this.userId, this.doctor, secret, moment(this.start).format('YYYYMMDDHHmm')].join(':');
    this.identifier = crypto.createHash('md5').update(toHash).digest('hex');
    next(callback);
});

module.exports = mongoose.model('Appointment', appointmentSchema);