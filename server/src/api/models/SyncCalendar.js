var mongoose = require('mongoose');

var syncCalSchema = new mongoose.Schema({
    doctorId: String,
    userId: String,
    events: Array
});

module.exports = mongoose.model('SyncCalendar', syncCalSchema);