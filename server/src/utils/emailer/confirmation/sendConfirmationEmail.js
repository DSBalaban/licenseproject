var nodemailer = require('nodemailer');
var services = require('nodemailer-wellknown');
var emailService = services('Hotmail');
var moment = require('moment');
var _ = require('lodash');

emailService.auth = {
    user: 'medtime.lpapp@outlook.com',
    pass: 'medtime7'
};

var transporter = nodemailer.createTransport(emailService);

function sendConfirmationEmail(to, appointment, host) {
    to = typeof to === 'string' ? to : to.join(', ');
    console.log(to);

    var appointmentConfirmationHtml = '' +
        '<h2> Appointment Confirmed </h2>' +
        '<h3> Please bring the following information to your appointment. Alternatively, make sure you know your exact email address!</h3>' +
        '<br>';

    var en0Network = require('os').networkInterfaces()['en0'];
    var ip = _.find(en0Network, {family: 'IPv4', internal: false}).address;
    console.log(ip);

    var uri = host.split('://')[0] + '://' + ip + ':8000/qrcode/generate/' +
        appointment.userId + '/' + appointment.doctor + '/' + moment(appointment.start).format('YYYYMMDDHHmm');

    appointmentConfirmationHtml += '<img src="' + uri + '">';
    appointmentConfirmationHtml += "<div> If you cannot see the provided image, " +
        "<a href='" + uri + "'>click here</a> and print or photograph the contents.</div>";

    var options = {
        from: '"MedTime LP" <noreply@medtime.com>',
        to: to,
        subject: 'Appointment Confirmation',
        html: appointmentConfirmationHtml
    };

    transporter.sendMail(options, function(error, info) {
        if(error) {
            console.error(error);
            return;
        }

        console.log('Message sent: ' + info.response);
    });
}

exports = module.exports = sendConfirmationEmail;