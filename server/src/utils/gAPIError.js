var _ = require('lodash');

function googleAPIError(res, err, reject) {
    console.log(err);
    res.status(err.code).json({
        message: error
    });

    if(reject && _.isFunction(reject)) reject(error);
}

exports = module.exports = googleAPIError;