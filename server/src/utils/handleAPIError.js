function handleError(error, res) {
    console.error(error.message);

    res.status(500).json({
        message: error.message
    });
}

exports = module.exports = handleError;