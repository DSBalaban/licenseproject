var calendarService = require.main.require('./src/api/services/google/calendar');

function getOAuth2(tokens) {
    var oauth2Client = calendarService.getAuthClient();
    oauth2Client.setCredentials({
        refresh_token: tokens.refreshToken
    });

    return oauth2Client;
}

exports = module.exports = getOAuth2;