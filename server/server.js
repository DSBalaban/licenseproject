var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = require('./src/api/models/User'),
    session = require('express-session'),
    MongoStore = require('connect-mongo')(session);

var localStrategy = require('./src/api/services/localStrategy');

var port = process.env.PORT || 8000;
var staticFilesRoot = path.join(__dirname, '../client/');

var app = express();

var mongoConn = mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/lpapp');

app.use(express.static(staticFilesRoot));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: 'hakunamatata',
    store: new MongoStore({
        mongooseConnection: mongoConn.connection
    })
}));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

passport.use('local-register', localStrategy.register);
passport.use('local-login', localStrategy.login);

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    next();
});

app.get('/', function(req, res) {
    res.sendFile("index.html", {
        root: path.join(staticFilesRoot, 'app/build')
    });
});

var server = app.listen(port, function() {
    console.log("Listening on port " + port);
});

module.exports = server;

// Initialize Socket.IO
var io = require('./src/config/sockets/io')(server);

// Routes
var auth = require('./src/config/routes/auth');
var clientAPI = require('./src/config/routes/client');
var googleAPI = require('./src/config/routes/google');
var syncAPI = require('./src/config/routes/sync');
var qrCodeAPI = require('./src/config/routes/qrcode');

app.use('/auth', auth);
app.use('/client', clientAPI, syncAPI);
app.use('/google', googleAPI.calendarAuth, googleAPI.calendarAPI);
app.use('/qrcode', qrCodeAPI);