var angular = require('angular');
var featuresPath = 'app/build/features';

angular
    .module('medTime')
    .config(routes);

routes.$inject = ['$stateProvider', '$urlRouterProvider'];

function routes($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('login', {
            url: '/',
            templateUrl: featuresPath + '/auth/login.html',
            controller: 'mtLoginCtrl',
            controllerAs: 'login'
        })
        .state('medtime', {
            url: '/medtime',
            templateUrl: featuresPath + '/medtime/medtime.html',
            controller: 'mtMainCtrl',
            controllerAs: 'main'
        })
        .state('medtime.home', {
            url: '^/home',
            views: {
                'content': {
                    templateUrl: featuresPath + '/home/home.html',
                    controller: 'mtHomeCtrl',
                    controllerAs: 'home'
                }
            }
        })
        .state('medtime.profile', {
            url: '^/profile',
            views: {
                'content': {
                    templateUrl: featuresPath + '/profile/profile.html',
                    controller: 'mtProfileCtrl',
                    controllerAs: 'profile',
                    resolve: {
                        userLoaded: function(mtUser) {
                            return mtUser.promise;
                        }
                    }
                }
            }
        })
        .state('medtime.calendar', {
            url: '^/calendar',
            views: {
                'content': {
                    templateUrl: featuresPath + '/calendar/calendar.html',
                    controller: 'mtCalendarCtrl',
                    controllerAs: 'calendar'
                }
            }
        })
        .state('medtime.appointments', {
            url: '^/appointments',
            views: {
                'content': {
                    templateUrl: featuresPath + '/appointments/appointments.html',
                    controller: 'mtAppointmentsCtrl',
                    controllerAs: 'appointments'
                }
            }
        });
}
