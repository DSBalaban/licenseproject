'use strict';

var angular = require('angular');
var constants = require('config/app.constants');

angular
    .module(constants.MAIN_MODULE_NAME)
    .config(themeConfig)
    .config(httpProviderConfig)
    .run(init);

themeConfig.$inject = ['$mdThemingProvider'];

function themeConfig($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('light-blue')
        .accentPalette('purple');

    $mdThemingProvider.theme('success-toast');
    $mdThemingProvider.theme('error-toast');
    $mdThemingProvider.theme('info-toast');
}

init.$inject = ['$state', '$rootScope', '$mdSidenav', '$auth', '$mdMedia', 'mtUser', 'mtSession'];

function init($state, $rootScope, $mdSidenav, $auth, $mdMedia, mtUser, mtSession) {
    $rootScope.$state = $state;
    $rootScope.$sidenav = $mdSidenav;
    $rootScope.$media = $mdMedia;
    $rootScope.logout = function() {
        $auth.logout().then(function() {
            $rootScope.$emit(constants.session.DESTROY);
            window.location.href = "/";
        });
    };
    mtSession.create();
}

httpProviderConfig.$inject = ['$httpProvider'];

function httpProviderConfig($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}