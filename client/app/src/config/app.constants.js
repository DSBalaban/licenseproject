var constants = {
    BASE_URL: 'http://localhost:8000',
    APP_NAME: 'MedTime',
    MAIN_MODULE_NAME: 'medTime',
    calendar: {
        CONNECTED: 'calendar-connected',
        CONNECTING: 'calendar-connecting',
        AUTHORIZED: 'calendar-auth-success',
        AUTHERROR: 'calendar-auth-error',
        SYNCERROR: 'calendar-events-retrieval-error'
    },
    events: {
        INSERT: 'event-insert',
        INSERT_ERROR: 'event-insert-error',
        INSERT_SUCCESS: 'event-insert-success',
        RECEIVE: 'events-receive',
        REMOVE: 'events-remove',
        EDIT: 'events-edit',
        SCHEDULE: 'events-schedule'
    },
    session: {
        INIT: 'initializeSession',
        DESTROY: 'destroySession'
    }
};

module.exports = constants;