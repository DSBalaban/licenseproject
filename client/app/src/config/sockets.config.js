var constants = require('./app.constants');
var angular = require('angular');

angular.module(constants.MAIN_MODULE_NAME).factory('mtSocket', function(mtUser) {
    var socket = require('socket.io-client')(constants.BASE_URL);

    function emit(eventName, data) {
        socket.emit(eventName, {user: mtUser.self.data, data: data});
    }

    socket.on('connected', function(data) {
        console.log(data.message, mtUser.self.data.firstname);
    });

    return {
        io: socket,
        emit: emit
    }
});
