'use strict';

require('./client.config.js');
require('./routes.config.js');
require('./app.constants');
require('./sockets.config');