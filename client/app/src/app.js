var angular = require('angular');
var constants = require('config/app.constants');

require('angular-material');
require('angular-messages');
require('angular-ui-router/release/angular-ui-router.js');
require('angular-material-icons');
require('satellizer');
require('ngstorage');

angular.module(constants.MAIN_MODULE_NAME, [
    'ngMaterial', 'ngMessages', 'ui.router', 'ngMdIcons', 'satellizer', 'ngAnimate', 'ngStorage'
]);

require('./config');
require('./features');