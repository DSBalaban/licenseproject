var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).controller('mtMainCtrl', mtMainCtrl);

mtMainCtrl.$inject = ['mtUser', 'mtAppointments'];

function mtMainCtrl(mtUser, mtAppointments) {
    var vm = this;

    vm.user = mtUser.self.data;
}