var angular = require('angular');
var constants = require('config/app.constants');

angular
    .module(constants.MAIN_MODULE_NAME)
    .controller('mtLoginCtrl', mtLoginCtrl);

mtLoginCtrl.$inject = ['$auth', '$state', 'mtUser', '$rootScope', 'mtToastr'];

function mtLoginCtrl($auth, $state, mtUser, $rootScope, mtToastr) {
    var vm = this;

    vm.user = { email: '', password: '' };

    vm.signUp = signUp;
    vm.login = login;
    vm.isAuthenticated = $auth.isAuthenticated;

    function signUp() {
        $auth.signup(vm.user).then(function(res) {
            login();
        }).catch(function(res) {
            console.error(res);
        });
    }

    function login() {
        $auth.login(vm.user).then(function(res) {
            $state.go('^.medtime.appointments');
            mtUser.reload();
        }).catch(function(res) {
            mtToastr.notify(res.data);
        });
    }
}