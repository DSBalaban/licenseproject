var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).factory('mtDoctors', mtDoctors);

mtDoctors.$inject = ['$http', '$q'];

function mtDoctors($http, $q) {
    var service = this;
    var deferred = $q.defer();

    service.list = [];
    service.listResolved = deferred.promise;
    service.selected = {
        doctor: null
    };

    function getList() {
        return $http.get('/client/doctors/').then(function(response) {
            service.list.push.apply(service.list, response.data.doctors);
            deferred.resolve();
        }).catch(function(error) {
            console.error(error);
            deferred.reject(error);
        });
    }

    getList();

    return {
        listResolved: service.listResolved,
        list: service.list,
        selected: service.selected,
        refresh: getList
    }
}