var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).directive('onEnter', onEnter);

function onEnter() {
    return {
        restrict: 'A',
        link: linkFn
    };

    function linkFn(scope, elm, attrs, ctrl) {
        elm.on('keydown keypress', function(event) {
            if(event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.onEnter);
                });
            }
        });
    }
}