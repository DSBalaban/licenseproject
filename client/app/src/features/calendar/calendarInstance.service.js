var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).service('mtCalInstance', mtCalInstance);

mtCalInstance.$inject = [];

function mtCalInstance() {
    this.elm = null;
}