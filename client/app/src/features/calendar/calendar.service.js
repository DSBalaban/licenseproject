var angular = require('angular');
var constants = require('config/app.constants');
var _ = require('lodash');
var moment = require('moment');

angular.module(constants.MAIN_MODULE_NAME).factory('mtCalendarService', mtCalendarService);

mtCalendarService.$inject = ['$window', 'mtSocket', 'mtUser', '$http'];

function mtCalendarService($window, mtSocket, mtUser, $http) {
    var service = this;

    service.config = {
        height: $window.innerHeight > 400 ? $window.innerHeight : 400
    };

    service.calendar = {
        data: null,
    };

    service.authPopup = {
        open: false,
        instance: null
    };

    mtSocket.io
        .on(constants.calendar.AUTHORIZED, authorizationCallback)
        .on(constants.calendar.AUTHORIZED, closePopup)
        .on(constants.calendar.AUTHERROR, closePopup);

    function initialize(width, height) {
        var wLeft = window.screenLeft || window.screenX;
        var wTop = window.screenTop || window.screenY;

        var left = wLeft + ((window.innerWidth - width) / 2);
        var top = wTop + ((window.innerHeight - height) / 2);

        var config = 'width='+width+',height='+height+',top='+top+',left='+left;

        service.authPopup.open = true;
        service.authPopup.instance = $window.open('/google/calendar-permissions/', 'Authorize Calendar Usage', config);
    }

    function closePopup() {
        if (service.authPopup.open) {
            service.authPopup.instance.close();
            service.authPopup.instance = null;
            service.authPopup.open = false;
        }
    }

    function authorizationCallback(data) {
        mtUser.self.data = data.user;
        mtUser.self.authorized.calendar = _.hasIn(data.user, 'googleServices.accessToken');

        initializeCalendar();
    }

    function initializeCalendar() {
        $http.post('/google/medtime-calendar/init', { user: mtUser.self.data })
            .then(function(response) {
                mtCalendarService.calendar.data = response.data.calendar;
            });
    }

    return {
        defaultConfig: service.config,
        calendar: service.calendar,
        authPopup: service.authPopup,
        initialize: initialize
    }
}