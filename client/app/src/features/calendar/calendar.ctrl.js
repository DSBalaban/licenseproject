var angular = require('angular');
var constants = require('config/app.constants');

var _ = require('lodash');

angular.module(constants.MAIN_MODULE_NAME).controller('mtCalendarCtrl', mtCalendarCtrl);

mtCalendarCtrl.$inject = ['mtUser', 'mtCalendarService'];

function mtCalendarCtrl(mtUser, mtCalendarService) {
    var vm = this;

    vm.eventSources = [];
    vm.config = mtCalendarService.defaultConfig;

    vm.initialize = mtCalendarService.initialize;
    vm.user = mtUser.self;

    vm.info = mtCalendarService.calendar;
    vm.info.loading = true;
}