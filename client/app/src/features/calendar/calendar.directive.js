var angular = require('angular');
var constants = require('config/app.constants');
var _ = require('lodash');
var moment = require('moment');

angular.module('medTime').directive('mtCalendar', mtCalendar);

mtCalendar.$inject = ['mtUser', 'mtEvents', 'mtSocket', 'mtCalSync', 'mtCalInstance'];

function mtCalendar(mtUser, mtEvents, mtSocket, mtCalSync, mtCalInstance) {
    return {
        restrict: 'EA',
        link: function (scope, elm, attrs, ctrl) {
            scope.$on('$destroy', function() {
                elm.fullCalendar('destroy');
                mtCalInstance.elm = null;
                
                mtSocket.io.off(constants.calendar.CONNECTED);
                mtSocket.io.off(constants.calendar.SYNCERROR);
            });

            mtSocket.io.emit(constants.calendar.CONNECTING, {user: mtUser.self.data});

            mtSocket.io
                .on(constants.calendar.CONNECTED, initialize)
                .on(constants.calendar.SYNCERROR, console.error);

            function initialize(data) {
                var events = mtEvents.parseEvents(data.items);

                elm.fullCalendar({
                    eventLimit: true,
                    allDaySlot: false,
                    minTime: '09:00:00',
                    maxTime: '18:00:00',
                    timezone: 'local',
                    dayClick: function(date, jsEvent, view) {
                        console.log(date, jsEvent, view);
                    },
                    selectable: true,
                    aspectRatio: 1.8,
                    weekends: false,
                    businessHours: {
                        start: '09:00',
                        end: '18:00'
                    },
                    eventConstraint: {
                        start: '09:00',
                        end: '18:00'
                    },
                    selectConstraint: "businessHours",
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    select: mtEvents.addEvents,
                    eventClick: function(event) {
                        var e = _.pick(event, [
                            'title', 'start', 'end', 'owner', 'googleId', 'isDoctorEvent',
                            'booked', 'bookedBy'
                        ]);

                        mtEvents.eventOnClick(e, mtUser.self.data);
                    },
                    events: events
                });

                mtCalInstance.elm = elm;

                if(!mtUser.self.data.isDoctor) {
                    mtCalSync.retrieve(mtUser.self.data).then(function(response) {
                        var doctorEvents = mtEvents.parseEvents(response.data.events, mtUser.self.data);

                        mtEvents.insertEvents(doctorEvents);
                    });
                }
            }
        }
    }
}