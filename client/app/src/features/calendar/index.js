'use strict';

require('./events');
require('./sync');
require('./calendar.ctrl');
require('./calendar.service');
require('./calendar.directive');
require('./calendarInstance.service');