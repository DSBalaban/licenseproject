var angular = require('angular');
var constants = require('config/app.constants');
var featuresPath = 'app/build/features';
var moment = require('moment');
var _ = require('lodash');

angular.module(constants.MAIN_MODULE_NAME).factory('mtEvents', mtEvents);

mtEvents.$inject = ['$mdDialog', 'mtSocket', 'mtCalInstance', 'mtToastr', 'mtUser', 'mtColors'];

function mtEvents($mdDialog, mtSocket, mtCalInstance, mtToastr, mtUser, mtColors) {
    mtSocket.io
        .on(constants.events.INSERT_SUCCESS, _insertGoogleEvent)
        .on(constants.events.RECEIVE, _insertGoogleEvents)
        .on(constants.events.EDIT, _editEvent)
        .on(constants.events.REMOVE, _removeEvent)
        .on(constants.events.SCHEDULE, _eventScheduled);

    function customizeEvent(start, end) {
        $mdDialog.show({
            controller: 'mtEventHelperCtrl',
            controllerAs: 'event',
            templateUrl: featuresPath + '/calendar/events/eventHelper.modal.html',
            parent: angular.element(document.body),
            locals: {
                event: {
                    start: start,
                    end: end
                }
            }
        }).then(function(events) {
            mtSocket.emit(constants.events.INSERT, events);
        });
    }

    function eventOnClick(event, user) {
        console.log(event.owner, user._id);
        console.info('Event booked: ', event.booked);
        console.info('Event booked by: ', event.bookedBy);
        if(event.owner === user._id) {
            editEvent(event);
        }else if(!event.booked){
            confirmAppointment(event);
        }else if(event.bookedBy === user._id) {
            var now = moment();
            var eventStart = moment(event.start);
            var message = '';
            if(now.isBefore(event.end) && now.isAfter(event.start)) {
                message = 'Appointment is currently ongoing.';
            }else if(now.isAfter(event.end)) {
                message = 'Appointment is in the past.';
            }else {
                message = 'Appointment is scheduled for the ' + eventStart.format('Do of MMMM') + '.';
            }

            mtToastr.simpleInfo(message);
        }else {
            mtToastr.simpleInfo('Event is booked by someone else.');
        }
    }

    function editEvent(event) {
        $mdDialog.show({
            controller: 'mtEventEdit',
            controllerAs: 'editor',
            templateUrl: featuresPath + '/calendar/events/eventEdit.modal.html',
            parent: angular.element(document.body),
            locals: {
                event: event
            }
        }).then(function(result) {
            var ioEvent = result.removed ? constants.events.REMOVE : constants.events.EDIT;
            
            if(result.edited || result.removed) 
                mtSocket.emit(ioEvent, result.event);
        });
    }

    function confirmAppointment(event) {
        var confirm = $mdDialog.confirm()
            .title('Appointment Confirmation')
            .textContent('' +
                'Schedule appointment from ' +
                event.start.format('HH:mm') +
                ' to ' +
                event.end.format('HH:mm') +
                ' on the ' +
                event.start.format('Do') +
                '?')
            .ok('Confirm')
            .cancel('Cancel');

        $mdDialog.show(confirm).then(function() {
            mtSocket.emit(constants.events.SCHEDULE, event);
        });
    }

    function createEvents(start, end, count, step, remainder, asDoctor, title) {
        var events = [];
        var s = start;

        for(var i = 0; i < count; i++) {
            events.push({
                summary: title,
                start: { dateTime: moment(s) },
                end: { dateTime: moment(s).add(step, 'hours') },
                isDoctorEvent: asDoctor
            });

            s.add(step, 'hours');
        }

        if(s.hours() < end.hours() && remainder) {
            events.push({
                summary: title,
                start: { dateTime: moment(s) },
                end: { dateTime: moment(end) },
                isDoctorEvent: asDoctor
            });
        }

        return events;
    }

    function __parseGoogleEvent(event) {
        var privateEventInfo = _.get(event, 'extendedProperties.private');
        var color = __getEventColor(privateEventInfo);

        return {
            title: event.summary,
            start: moment(event.start.dateTime),
            end: moment(event.end.dateTime),
            color: color,
            googleId: event.id,
            owner: privateEventInfo.owner,
            isDoctorEvent: privateEventInfo.isDoctorEvent === "true",
            booked: privateEventInfo.booked === "true",
            bookedBy: privateEventInfo.bookedBy
        }
    }

    function parseGoogleEvents(events) {
        return _.map(events, function(event) {
            return __parseGoogleEvent(event);
        });
    }

    function _insertGoogleEvent(event) {
        if(!mtCalInstance.elm) return;
        mtCalInstance.elm.fullCalendar('addEventSource', [__parseGoogleEvent(event)]);
    }

    function _insertGoogleEvents(response) {
        if(!mtCalInstance.elm) return;
        mtCalInstance.elm.fullCalendar('addEventSource', parseGoogleEvents(response.data));
    }

    function insertEvents(events) {
        if(!mtCalInstance.elm) return;
        mtCalInstance.elm.fullCalendar('addEventSource', events);
    }

    function _editEvent(response) {
        if(!mtCalInstance.elm) return;
        var event = mtCalInstance.elm.fullCalendar('clientEvents', __filter(response.data))[0];

        angular.extend(event, response.data);
        mtCalInstance.elm.fullCalendar('updateEvent', event);
    }

    function _removeEvent(response) {
        if(!mtCalInstance.elm) return;
        mtCalInstance.elm.fullCalendar('removeEvents', __filter(response.data));
    }

    function _eventScheduled(scheduledEvent) {
        var privateEventInfo = _.pick(scheduledEvent, ['isDoctorEvent', 'booked', 'owner', 'bookedBy']);
        scheduledEvent.color = __getEventColor(privateEventInfo);

        _editEvent({data: scheduledEvent});
    }

    function __filter(event) {
        return function(e) {
            return e.googleId === event.googleId;
        }
    }

    function __getEventColor(privateEventInfo) {
        var isDoctorEvent = _.get(privateEventInfo, 'isDoctorEvent');
        if(isDoctorEvent && isDoctorEvent.toString() === "true") {
            var booked = privateEventInfo.booked;
            var bookedByCurrentUser = mtUser.self.data._id === privateEventInfo.bookedBy;
            return booked ? bookedByCurrentUser ? mtColors.events.bookedByMe : mtColors.events.booked : mtColors.events.available;
        } else {
            return mtColors.events.personal;
        }
    }

    return {
        customizeEvent: customizeEvent,
        createEvents: createEvents,
        parseEvents: parseGoogleEvents,
        addEvents: customizeEvent,
        insertEvents: insertEvents,
        eventOnClick: eventOnClick
    }
}