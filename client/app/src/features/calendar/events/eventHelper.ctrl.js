var angular = require('angular');
var constants = require('config/app.constants');
var humanize = require('humanize-plus');
var moment = require('moment');

angular.module(constants.MAIN_MODULE_NAME).controller('mtEventHelperCtrl', mtEventHelperCtrl);

mtEventHelperCtrl.$inject = ['event', '$mdDialog', 'mtEvents', 'mtUser'];

function mtEventHelperCtrl(event, $mdDialog, mtEvents, mtUser) {
    var vm = this;

    vm.title = 'Custom Event';
    vm.user = mtUser.self.data;
    vm.selectedHours = event.end.diff(event.start, 'hours') || 0.5;
    vm.applyRest = false;
    vm.asDoctor = false;
    vm.hours = {
        min: 0.5,
        max: vm.selectedHours
    };

    vm.eventsNumberToCreate = eventsNumberToCreate;
    vm.createSingleEvent = createSingleEvent;
    vm.createMultipleEvents = createMultipleEvents;
    vm.remainingHours = remainingHours;
    vm.pluralize = pluralize;
    vm.apply = apply;
    vm.showApplyRest = showApplyRest;

    function eventsNumberToCreate() {
        return ~~(vm.hours.max / vm.selectedHours);
    }

    function createSingleEvent() {
        return Math.ceil(vm.selectedHours - 0.5) === vm.hours.max || vm.hours.min === vm.hours.max;
    }

    function createMultipleEvents() {
        return Math.ceil(vm.selectedHours - 0.5) < vm.hours.max && vm.hours.min !== vm.hours.max;
    }

    function remainingHours() {
        return vm.hours.max % vm.selectedHours;
    }

    function showApplyRest() {
        var restExists = remainingHours() !== 0;
        vm.applyRest = vm.applyRest && restExists;
        return restExists;
    }

    function pluralize(number, string) {
        return number === 1.5 ? 'hours' : humanize.pluralize(number, string);
    }

    function apply() {
        var evenEventsCount = eventsNumberToCreate();
        var events = mtEvents.createEvents(event.start, event.end, evenEventsCount, vm.selectedHours, vm.applyRest, vm.asDoctor, vm.title);

        $mdDialog.hide(events);
    }
}