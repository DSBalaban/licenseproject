var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).service('mtColors', mtColors);

mtColors.$inject = [];

function mtColors() {
    this.events = {
        bookedByMe: '#E91E63',
        booked: '#f44336',
        available: '#4CAF50',
        personal: '#2196F3'
    }
}