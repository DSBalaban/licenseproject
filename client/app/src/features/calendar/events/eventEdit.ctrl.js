var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).controller('mtEventEdit', mtEventEdit);

mtEventEdit.$inject = ['event', '$mdDialog'];

function mtEventEdit(event, $mdDialog) {
    var vm = this;
    var initialEventInfo = angular.extend({}, event);

    console.log(event);

    vm.event = event;
    vm.cancel = cancel;
    vm.apply = apply;

    function apply(shouldRemove) {
        var edited = false;
        if(!shouldRemove) {
            edited = !angular.equals(event, initialEventInfo);
        }

        $mdDialog.hide({
            removed: shouldRemove,
            edited: edited,
            event: event
        });
    }
    
    function cancel() {
        $mdDialog.cancel();
    }
}