var angular = require('angular');
var constants = require('config/app.constants');
var _ = require('lodash');

angular.module(constants.MAIN_MODULE_NAME).factory('mtCalSync', mtCalSync);

mtCalSync.$inject = ['$http', '$localStorage', 'mtToastr'];

function mtCalSync($http, $localStorage, mtToastr) {
    function sync(user, doctor) {
        var payload = { doctor: doctor, user: user};
        return $http.post('/google/medtime-calendar/sync/', payload).then(function(response) {
            if(response.data.success) $localStorage[user._id] = doctor;
            mtToastr.notify(response.data);
        });
    }

    function retrieveSlots(user) {
        var doctor = $localStorage[user._id];
        var payload = {
            params: {
                userId: user._id,
                doctorId: doctor._id
            }
        };

        return $http.get('/client/synchronized-events', payload);
    }

    return {
        sync: sync,
        retrieve: retrieveSlots
    }
}