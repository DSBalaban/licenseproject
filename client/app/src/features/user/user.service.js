var angular = require('angular');
var constants = require('config/app.constants');
var _ = require('lodash');

angular.module(constants.MAIN_MODULE_NAME)
    .factory('mtUser', mtUser);

mtUser.$inject = ['$auth', '$http', '$q', '$rootScope'];

function mtUser($auth, $http, $q, $rootScope) {
    var service = {};
    var deferred = $q.defer();

    service.self = {
        data: {
            isDoctor: false,
            setup: false
        },
        authorized: {
            calendar: false
        }
    };

    service.userPayload = $auth.getPayload();
    service.userLoaded = deferred.promise;

    if(service.userPayload) getUser();

    function getUser() {
        service.userPayload = $auth.getPayload();
        if(!service.userPayload) {
            console.info('User session does not exist.');
            // deferred.reject('User session does not exist.');
            return;
        }

        $http.get('/client/user/' + service.userPayload.sub)
            .then(function(response) {
                service.self.data = response.data.user;
                service.self.authorized.calendar = !!_.has(service.self.data, 'googleServices.accessToken');

                $rootScope.$broadcast(constants.session.INIT, service.self.data);

                deferred.resolve(service.self);
            })
            .catch(function(error) {
                console.error(error);
                deferred.reject();
            });
    }

    function updateUser(payload) {
        return $http.post('/client/user-setup/', payload).then(function(response) {
            angular.extend(service.self.data, response.data.user);
        }).catch(function(error) {
            alert(error.message);
        });
    }

    return {
        self: service.self,
        update: updateUser,
        reload: getUser,
        promise: service.userLoaded
    };
}