var angular = require('angular');
var constants = require('config/app.constants');
var _ = require('lodash');

angular.module(constants.MAIN_MODULE_NAME).factory('mtSession', mtSession);

mtSession.$inject = ['$rootScope', 'mtUser', 'mtSocket', 'mtToastr'];

function mtSession($rootScope, mtUser, mtSocket, mtToastr) {
    mtSocket.io
        .on(constants.events.RECEIVE, eventsAdded)
        .on(constants.events.EDIT, eventEdited)
        .on(constants.events.REMOVE, eventRemoved);

    function eventsAdded(response) {
        if(!response.notify) return;
        var message = "Some events were added to your doctor's calendar.";

        if(response.data.length === 1) {
            message = "Event '" + response.data[0].summary + "' was added to your doctor's calendar.";
        }

        mtToastr.simpleInfo(message);
    }

    function eventRemoved(response) {
        if(!response.notify) return;
        mtToastr.simpleInfo("Event '" + response.data.title + "' was removed from your doctor's calendar.");
    }

    function eventEdited(response) {
        if(!response.notify) return;
        mtToastr.simpleInfo("An event was edited in your doctor's calendar.");
    }

    function createSessionHandlers() {
        $rootScope.$on(constants.session.INIT, function(event, userData) {
            console.info('Initializing Session');

            mtUser.self.data = userData;
            mtUser.self.authorized.calendar = !!_.has(userData, 'googleServices.accessToken');
            mtSocket.io.emit('join', mtUser.self.data);
        });

        $rootScope.$on(constants.session.DESTROY, function() {
            console.info('Destroying Session');

            mtUser.self.data = null;
            mtUser.self.authorized.calendar = false;
        });
    }

    return {
        create: createSessionHandlers
    }
}