var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).directive('transitionOverflow', transitionOverflow);

function transitionOverflow() {
    return {
        restrict: 'A',
        link: function(scope, elem) {
            scope.$on('$stateChangeStart', function() {
                elem[0].style.overflow = 'hidden';
            });

            scope.$on('$stateChangeSuccess', function() {
                setTimeout(function() {
                    elem[0].style.overflow = 'auto';
                }, 750);
            });
        }
    }
}