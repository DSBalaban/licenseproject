var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).factory('mtToastr', mtToastr);

mtToastr.$inject = ['$mdToast'];

function mtToastr($mdToast) {
    function toast(data) {
        var toast = $mdToast.simple().textContent(data.message);

        data.success ? toast.theme('success-toast') : toast.theme('error-toast');

        return $mdToast.show(toast);
    }

    function simpleInfo(message) {
        var toast = $mdToast.simple().theme('info-toast').textContent(message);
        $mdToast.show(toast);
    }

    return {
        notify: toast,
        simpleInfo: simpleInfo
    }
}