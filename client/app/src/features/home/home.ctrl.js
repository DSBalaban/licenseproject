var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME)
    .controller('mtHomeCtrl', mtHomeCtrl);

mtHomeCtrl.$inject = ['mtDoctors', 'mtCalSync', 'mtUser'];

function mtHomeCtrl(mtDoctors, mtCalSync, mtUser) {
    var vm = this;

    vm.doctors = mtDoctors.list;
    vm.selected = mtDoctors.selected;
    
    vm.syncWith = function(doctor) {
        mtCalSync.sync(mtUser.self.data, doctor);
    }
}