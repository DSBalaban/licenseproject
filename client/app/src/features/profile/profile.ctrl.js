var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).controller('mtProfileCtrl', mtProfileCtrl);

mtProfileCtrl.$inject = ['mtUser', '$state'];

function mtProfileCtrl(mtUser, $state) {
    var vm = this;

    vm.user = mtUser.self.data;
    vm.setup = setup;

    function setup() {
        mtUser.update(vm.user);
        $state.go('^.home')
    }
}