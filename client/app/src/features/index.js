'use strict';

require('./auth');
require('./user');
require('./doctor');
require('./medtime');
require('./home');
require('./utils');
require('./profile');
require('./calendar');
require('./keyevents');
require('./toastr');
require('./appointments');
