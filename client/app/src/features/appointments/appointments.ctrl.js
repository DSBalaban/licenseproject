var angular = require('angular');
var constants = require('config/app.constants');
var moment = require('moment');

angular.module(constants.MAIN_MODULE_NAME).controller('mtAppointmentsCtrl', mtAppointmentsCtrl);

mtAppointmentsCtrl.$inject = ['mtAppointments'];

function mtAppointmentsCtrl(mtAppointments) {
    var vm = this;

    vm.list = mtAppointments.list;
    vm.getHumanizedHours = getHumanizedHours;
    vm.getHumanizedDay = getHumanizedDay;
    vm.humanTime = humanTime;

    function humanTime(date) {
        return moment(date).calendar();
    }

    function getHumanizedHours(date) {
        return moment(date).format('HH:mm');
    }

    function getHumanizedDay() {
        return moment(date).format('');
    }
}