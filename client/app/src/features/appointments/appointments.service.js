var angular = require('angular');
var constants = require('config/app.constants');

angular.module(constants.MAIN_MODULE_NAME).factory('mtAppointments', mtAppointments);

mtAppointments.$inject = ['$http', 'mtToastr', '$state'];

function mtAppointments($http, mtToastr, $state) {
    var service = this;
    
    service.data = {
        list: [],
        retrieved: false
    };
    
    service.retrieve = function() {
        return $http.get('/client/upcoming-appointments/').then(function(response) {
            Array.prototype.push.apply(service.data.list, (response.data.appointments || []));
            service.data.retrieved = true;

            if($state.current.name !== 'medtime.appointments') {
                mtToastr.simpleInfo(response.data.message);
            }
        });
    };

    service.retrieve();

    return {
        retrieve: service.retrieve,
        list: service.data.list,
        retrieved: service.data.retrieved
    }
}