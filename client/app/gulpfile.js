var gulp = require('gulp'),
    jade = require('gulp-jade'),
    source = require('vinyl-source-stream'),
    plumber = require('gulp-plumber'),
    browserify = require('browserify'),
    gutil = require('gulp-util');

var jadeGlobPatterns = ['./src/**/*.jade', '!./src/{partials,partials/**}', '!./src/**/{partials,partials/**}', '!./src/server-side-views/**'];

gulp.task('templates', function() {
    var jadeConfig = {
        pretty: true
    };

    gulp.src(jadeGlobPatterns)
        .pipe(plumber())
        .pipe(jade(jadeConfig))
        .on('error', gutil.log)
        .pipe(gulp.dest('./build/'));
});

gulp.task('watch', function() {
    gulp.watch('./src/**/*.jade', ['templates']);
    gulp.watch('./src/**/*.js', ['browserify'])
});

gulp.task('browserify', function() {
    var config = {
        sourceFile: './src/app.js',
        destFile: 'app-bundle.js',
        destFolder: './build'
    };

    var bundler = browserify({
        entries: [config.sourceFile],
        paths: ['./src'],
        debug: true
    }).on('error', gutil.log);

    var stream = bundler.bundle().on('error', gutil.log);

    return stream.pipe(plumber())
        .pipe(source(config.destFile))
        .pipe(gulp.dest(config.destFolder));
});

gulp.task('build', ['templates', 'browserify']);