#!/usr/bin/env bash

client_dir=`pwd`

cd style && compass compile && compass watch & cd $client_dir/app&&
npm run gulp-build && npm run gulp watch